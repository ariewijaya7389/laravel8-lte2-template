{{-- @extends('layouts.app')

@section('sidebar') --}}


<ul class="sidebar-menu" data-widget="tree">
    <li class="header">MAIN NAVIGATION</li>

    @foreach(App\Models\Menu::orderBy('index','asc')->get() as $menuItem)

        @if( $menuItem->parent == 0 )
            <li {{ $menuItem->url ? '' : "class=treeview" }}>
            <a href="{{ $menuItem->children->isEmpty() ? url($menuItem->url) : "#" }}"{{ $menuItem->children->isEmpty() ? '' : "" }}>
                <i class="fa fa-{{$menuItem->icon}}"></i> <span>
                    {{ $menuItem->name }}</span>
                @if (!$menuItem->url)
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                @endif
            </a>
        @endif

        @if( ! $menuItem->children->isEmpty() )
        <ul class="treeview-menu" >
            @foreach($menuItem->children as $subMenuItem)
                <li><a href='{{ url($subMenuItem->url) }}'><i class="fa fa-circle-o"></i> {{ $subMenuItem->name }}</a></li>
            @endforeach
            </ul>
        @endif
        </li>

        @endforeach

    {{-- <li class="treeview">
        <a href="#">
          <i class="fa fa-share"></i> <span>Multilevel</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="#"><i class="fa fa-circle-o"></i> Level One</a></li>
          <li class="treeview">
            <a href="#"><i class="fa fa-circle-o"></i> Level One
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="#"><i class="fa fa-circle-o"></i> Level Two</a></li>
              <li class="treeview">
                <a href="#"><i class="fa fa-circle-o"></i> Level Two
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                  <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                </ul>
              </li>
            </ul>
          </li>
          <li><a href="#"><i class="fa fa-circle-o"></i> Level One</a></li>
        </ul>
    </li>

    <li class="treeview">
        <a href="#">
          <i class="fa fa-edit"></i> <span>Forms</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href='{{asset("forms/general.html")}}'><i class="fa fa-circle-o"></i> General Elements</a></li>
          <li><a href='{{asset("forms/advanced.html")}}'><i class="fa fa-circle-o"></i> Advanced Elements</a></li>
          <li><a href='{{asset("forms/editors.html")}}'><i class="fa fa-circle-o"></i> Editors</a></li>
        </ul>
    </li> --}}


    {{-- <li class="treeview">
      <a href="#">
        <i class="fa fa-files-o"></i>
        <span>Layout Options</span>
        <span class="pull-right-container">
          <span class="label label-primary pull-right">4</span>
        </span>
      </a>
      <ul class="treeview-menu">
        <li><a href='{{asset("layout/top-nav.html")}}'><i class="fa fa-circle-o"></i> Top Navigation</a></li>
        <li><a href='{{asset("layout/boxed.html")}}'><i class="fa fa-circle-o"></i> Boxed</a></li>
        <li><a href='{{asset("layout/fixed.html")}}'><i class="fa fa-circle-o"></i> Fixed</a></li>
        <li><a href='{{asset("layout/collapsed-sidebar.html")}}'><i class="fa fa-circle-o"></i> Collapsed Sidebar</a></li>
      </ul>
    </li>
     <li class="treeview">
        <a href="#">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>
        <ul class="treeview-menu">
            <li><a href='{{asset("index.html")}}'><i class="fa fa-circle-o"></i> Dashboard v1</a></li>
            <li><a href='{{asset("index2.html")}}'><i class="fa fa-circle-o"></i> Dashboard v2</a></li>
        </ul>
    </li>
    <li>
      <a href='{{asset("widgets.html")}}'>
        <i class="fa fa-th"></i> <span>Widgets</span>
        <span class="pull-right-container">
          <small class="label pull-right bg-green">Hot</small>
        </span>
      </a>
    </li>
    <li class="treeview">
      <a href="#">
        <i class="fa fa-pie-chart"></i>
        <span>Charts</span>
        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
      </a>
      <ul class="treeview-menu">
        <li><a href='{{asset("charts/chartjs.html")}}'><i class="fa fa-circle-o"></i> ChartJS</a></li>
        <li><a href='{{asset("charts/morris.html")}}'><i class="fa fa-circle-o"></i> Morris</a></li>
        <li><a href='{{asset("charts/flot.html")}}'><i class="fa fa-circle-o"></i> Flot</a></li>
        <li><a href='{{asset("charts/inline.html")}}'><i class="fa fa-circle-o"></i> Inline charts</a></li>
      </ul>
    </li>
    <li class="treeview">
      <a href="#">
        <i class="fa fa-laptop"></i>
        <span>UI Elements</span>
        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
      </a>
      <ul class="treeview-menu">
        <li><a href='{{asset("UI/general.html")}}'><i class="fa fa-circle-o"></i> General</a></li>
        <li><a href='{{asset("UI/icons.html")}}'><i class="fa fa-circle-o"></i> Icons</a></li>
        <li><a href='{{asset("UI/buttons.html")}}'><i class="fa fa-circle-o"></i> Buttons</a></li>
        <li><a href='{{asset("UI/sliders.html")}}'><i class="fa fa-circle-o"></i> Sliders</a></li>
        <li><a href='{{asset("UI/timeline.html")}}'><i class="fa fa-circle-o"></i> Timeline</a></li>
        <li><a href='{{asset("UI/modals.html")}}'><i class="fa fa-circle-o"></i> Modals</a></li>
      </ul>
    </li>
    <li class="treeview">
      <a href="#">
        <i class="fa fa-edit"></i> <span>Forms</span>
        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
      </a>
      <ul class="treeview-menu">
        <li><a href='{{asset("forms/general.html")}}'><i class="fa fa-circle-o"></i> General Elements</a></li>
        <li><a href='{{asset("forms/advanced.html")}}'><i class="fa fa-circle-o"></i> Advanced Elements</a></li>
        <li><a href='{{asset("forms/editors.html")}}'><i class="fa fa-circle-o"></i> Editors</a></li>
      </ul>
    </li>
    <li class="treeview">
      <a href="#">
        <i class="fa fa-table"></i> <span>Tables</span>
        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
      </a>
      <ul class="treeview-menu">
        <li><a href='{{asset("tables/simple.html")}}'><i class="fa fa-circle-o"></i> Simple tables</a></li>
        <li><a href='{{asset("tables/data.html")}}'><i class="fa fa-circle-o"></i> Data tables</a></li>
      </ul>
    </li>
    <li>
      <a href='{{asset("calendar.html")}}'>
        <i class="fa fa-calendar"></i> <span>Calendar</span>
        <span class="pull-right-container">
          <small class="label pull-right bg-red">3</small>
          <small class="label pull-right bg-blue">17</small>
        </span>
      </a>
    </li>
    <li>
      <a href='{{asset("mailbox/mailbox.html")}}'>
        <i class="fa fa-envelope"></i> <span>Mailbox</span>
        <span class="pull-right-container">
          <small class="label pull-right bg-yellow">12</small>
          <small class="label pull-right bg-green">16</small>
          <small class="label pull-right bg-red">5</small>
        </span>
      </a>
    </li>
    <li class="treeview active">
      <a href="#">
        <i class="fa fa-folder"></i> <span>Examples</span>
        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
      </a>
      <ul class="treeview-menu">
      </ul>
    </li>

    <li><a href="https://adminlte.io/docs"><i class="fa fa-book"></i> <span>Documentation</span></a></li>
    <li class="header">LABELS</li>
    <li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Important</span></a></li>
    <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>Warning</span></a></li>
    <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Information</span></a></li> --}}

  </ul>

  {{-- @endsection --}}
