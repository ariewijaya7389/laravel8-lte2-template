<?php

use App\Http\Controllers\Auth\LoginController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});


Auth::routes();




Route::group(['middleware' => ['auth']], function () {
    Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
    Route::get('/logout', [LoginController::class, 'logout'])->name('logout');

    // Route::prefix('dashboard')->group(function () {
    //     Route::get('/', [DashboardController::class, 'index'])->name('dashboard.index');
    //     Route::post('/', [DashboardController::class, 'index'])->name('dashboard.index');
    //     Route::get('/checkPilihNama/{id}', [DashboardController::class, 'checkPilihNama'])->name('dashboard.checkPilihNama');
    //     Route::get('/data/{id}', [DashboardController::class, 'data'])->name('dashboard.data');
    //     Route::get('/show/{id}', [DashboardController::class, 'show'])->name('dashboard.show');
    //     Route::post('/store', [DashboardController::class, 'store'])->name('dashboard.store');
    // });

    // Route::prefix('laporan')->group(function () {
    //     Route::post('/laporan', [LaporanController::class, 'laporan'])->name('laporan');

    // });

    // Route::prefix('mytask')->group(function () {
    //     Route::get('/', [MyTaskController::class, 'index'])->name('mytask.index');
    //     Route::post('/', [MyTaskController::class, 'index'])->name('mytask.index');
    //     Route::get('/actionData/{id}', [MyTaskController::class, 'actionData'])->name('mytask.actionData');
    //     Route::post('/action', [MyTaskController::class, 'action'])->name('mytask.action');
    //     Route::get('/notifCounter', [MyTaskController::class, 'notifCounter'])->name('mytask.notifCounter');

    // });

    Route::prefix('master')->group(function () {
        Route::prefix('roles')->group(function () {
            Route::get('/', [RolesController::class, 'index'])->name('master.roles.index');
            Route::post('/store', [RolesController::class, 'store'])->name('master.roles.store');
            Route::get('/data', [RolesController::class, 'data'])->name('master.roles.data');
            Route::get('/show', [RolesController::class, 'show'])->name('master.roles.show');
            Route::get('/edit/{id}', [RolesController::class, 'edit'])->name('master.roles.edit');
            Route::get('/destroy{id}', [RolesController::class, 'destroy'])->name('master.roles.destroy');
        });

        Route::prefix('job')->group(function () {
            Route::get('/', [JobController::class, 'index'])->name('master.job.index');
            Route::post('/store', [JobController::class, 'store'])->name('master.job.store');
            Route::get('/data', [JobController::class, 'data'])->name('master.job.data');
            Route::get('/edit/{id}', [JobController::class, 'edit'])->name('master.job.edit');
            Route::get('/destroy/{id}', [JobController::class, 'destroy'])->name('master.job.destroy');
        });

        Route::prefix('flow')->group(function () {
            Route::get('/', [FlowController::class, 'index'])->name('master.flow.index');
            Route::get('/data', [FlowController::class, 'data'])->name('master.flow.data');
            Route::get('/edit/{id}', [FlowController::class, 'edit'])->name('master.flow.edit');
            Route::post('/store', [FlowController::class, 'store'])->name('master.flow.store');
        });

        Route::prefix('jabatan')->group(function () {
            Route::get('/', [JabatanController::class, 'index'])->name('master.jabatan.index');
            Route::post('/store', [JabatanController::class, 'store'])->name('master.jabatan.store');
            Route::get('/data', [JabatanController::class, 'data'])->name('master.jabatan.data');
            Route::get('/show', [JabatanController::class, 'show'])->name('master.jabatan.show');
            Route::get('/edit/{id}', [JabatanController::class, 'edit'])->name('master.jabatan.edit');
            Route::get('/destroy{id}', [JabatanController::class, 'destroy'])->name('master.jabatan.destroy');
        });

        Route::prefix('user')->group(function () {
            Route::get('/', [UserController::class, 'index'])->name('master.user.index');
            Route::post('/store', [UserController::class, 'store'])->name('master.user.store');
            Route::get('/data', [UserController::class, 'data'])->name('master.user.data');
            Route::get('/show', [UserController::class, 'show'])->name('master.user.show');
            Route::get('/edit/{id}', [UserController::class, 'edit'])->name('master.user.edit');
            Route::get('/destroy{id}', [UserController::class, 'destroy'])->name('master.user.destroy');
        });
    });

    // Route::prefix('password')->group(function () {
    //     Route::get('/', [PasswordController::class, 'index'])->name('password.index');
    //     Route::post('/update', [PasswordController::class, 'update'])->name('password.change');


    // });

});
